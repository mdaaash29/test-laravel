API Endpoints:
/api/auth/...

    Route::post('login','AuthController@login');
    Route::post('register','AuthController@register');
    Route::post('logout','AuthController@logout');
    Route::get('profile','AuthController@profile');
    Route::post('refresh','AuthController@refresh');


Login
===============
/api/auth/login: POST method.
Required post parameters email and password

Register
===============
/api/auth/register: POST method.
Required post parameters name, email and password.

(with auth JWT Bearer-Token Get from login method)

Profile
===============

/api/auth/profile: GET method.

logout
===============

/api/auth/logout: POST method.

refresh
===============
/api/auth/refresh: POST method.

 Barang
 ==============================
    Route::get('getKategori','BarangController@getKategori');
    Route::get('getBarang','BarangController@getBarang');
    Route::get('getBarang/{id}','BarangController@getBarangById');
    Route::get('getLaporanStock','BarangController@getLaporanStock');
    Route::get('getLaporanStock/{id}','BarangController@getLaporanStockById');
    Route::post('addBarang','BarangController@addBarang');
    Route::put('updateStock/{id}','BarangController@updateStock');
    Route::get('getLaporanBarang','BarangController@getLaporanBarang');
    Route::get('getLaporanBarang/{id}','BarangController@getLaporanBarangById');



(with auth JWT Bearer-Token Get from login method)


role:admin
==============================

getKategori
===============

(lihat daftar kategori)
/api/getKategori: GET method.

getBarang
===============

(lihat daftar barang dan stok)
/api/getBarang: GET method.

getBarangById
===============

(lihat daftar barang by id) 
/api/getBarang/{id}: GET method.
Required get parameters id_barang

role: Staff Gudang
==============================

addBarang
===============

(tambah barang)

/api/addBarang: POST method.
Required post parameters nama_barang,id_kategori_barang,stok

updateStock
===============

(tambah/kurang stock barang)

/api/updateStock/{id}: PUT method.
Required put parameters id_barang,
Required put body stock

ex:{
"stok" = 10
}
stok = number, - for taking the stock (ex: -10), dor adding the stock just the number(ex: 20)

role: Admin

getLaporanStock
===============

(lihat laporan stok)

/api/getLaporanStock/: GET method.
Optional get parameters filter_date
params: filter_date = 1. ('which day')
               2. ('this week')
               3. ('which month')
               4. ('which year')

if param date = 1, add another params date = "number, which date from 1-31"
if param date = 2, add another params week = "number, which week from 1-4"
if param date = 3, add another params month = "number, which month from 1-12"
if param date = 4, add another params year = "number, which year ex:2022"


getLaporanBarang
===============

(lihat laporan barang)

/api/getLaporanBarang/: GET method.
Optional get parameters filter_date
params: filter_date = 1. ('which day')
               2. ('this week')
               3. ('which month')
               4. ('which year')

if param date = 1, add another params date = "number, which date from 1-31"
if param date = 2, add another params week = "number, which week from 1-4"
if param date = 3, add another params month = "number, which month from 1-12"
if param date = 4, add another params year = "number, which year ex:2022"



getLaporanStockById
====================

(lihat laporan stok by id)

/api/getLaporanStock/{id}: GET method.
Required get parameters id_barang 


getLaporanBarangById
====================

(lihat laporan barang by id)

/api/getLaporanBarang/{id}: GET method.
Required get parameters id_barang

