<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    'prefix' => 'auth',

], function($router){
    Route::post('login','AuthController@login');
    Route::post('register','AuthController@register');
    Route::post('logout','AuthController@logout');
    Route::get('profile','AuthController@profile');
    Route::post('refresh','AuthController@refresh');
});

Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',

], function($router){
    Route::resource('todos','TodoController');
});

Route::group([
    'middleware' => 'api',
    'namespace' => 'App\Http\Controllers',
    
], function($router){
    Route::get('getKategori','BarangController@getKategori');
    Route::get('getBarang','BarangController@getBarang');
    Route::get('getBarang/{id}','BarangController@getBarangById');
    Route::get('getLaporanStock','BarangController@getLaporanStock');
    Route::get('getLaporanStock/{id}','BarangController@getLaporanStockById');
    Route::post('addBarang','BarangController@addBarang');
    Route::put('updateStock/{id}','BarangController@updateStock');
    Route::get('getLaporanBarang','BarangController@getLaporanBarang');
    Route::get('getLaporanBarang/{id}','BarangController@getLaporanBarangById');
});


