<?php

namespace App\Http\Controllers;

use App\Models\Barang;
use App\Models\Kategori;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Carbon\Carbon;



class BarangController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
        $this->user = $this->guard()->user();

    }//end __construct()

    public function getKategori()
    {
        return response()->json(Kategori::All(),200);
    }
    
    public function getBarang()
    {

        $barang = DB::table('barangs')->leftJoin('kategoris', 'barangs.id_kategori_barang', '=', 'kategoris.id_kategori_barang')->get();
        return response()->json($barang,200);

    }

    public function getLaporanStock(Request $request)
    {

        $checking_role = $this->guard()->user();

        if ($checking_role->role == 0) {
            return response()->json([
                'message'=>'Unauthorized'
            ],401);
        }

        $query = DB::table('barangs')->leftJoin('kategoris', 'barangs.id_kategori_barang', '=', 'kategoris.id_kategori_barang');
        if ($request->filter_date) {

            if ($request->date && $request->filter_date == 1) {
                $query->whereDay('barangs.created_at', $request->date);
            }
            elseif ($request->filter_date == 2) {
                $query->whereBetween('barangs.created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
            }
            elseif ($request->month && $request->filter_date == 3) {
                $query->whereMonth('barangs.created_at', $request->month);
            }
            elseif ($request->year && $request->filter_date == 4) {
                $query->whereMonth('barangs.created_at', $request->year);
            }
        }

        $barang = $query->get();
        return response()->json($barang,200);

    }


    public function getBarangById($id)
    {

        $barang =  DB::table('barangs')->leftJoin('kategoris', 'barangs.id_kategori_barang', '=', 'kategoris.id_kategori_barang')->where('barangs.id_barang', $id)->get();
        if ($barang->isEmpty()) {
            return response()->json([
                'message'=>'Barang Tidak ditemukan'
            ],404);
        }
        return response()->json($barang,200);
    }

    public function getLaporanStockById($id)
    {

        $checking_role = $this->guard()->user();

        if ($checking_role->role == 0) {
            return response()->json([
                'message'=>'Unauthorized'
            ],401);
        }

        $barang =  DB::table('barangs')->leftJoin('kategoris', 'barangs.id_kategori_barang', '=', 'kategoris.id_kategori_barang')->where('barangs.id_barang', $id)->get();
        if ($barang->isEmpty()) {
            return response()->json([
                'message'=>'Barang Tidak ditemukan'
            ],404);
        }
        return response()->json($barang,200);
    }

    public function getLaporanBarang(Request $request)
    {
        $checking_role = $this->guard()->user();

        if ($checking_role->role == 0) {
            return response()->json([
                'message'=>'Unauthorized'
            ],401);
        }
        // $barang = DB::table('laporan_barangs')->get();
        $query = DB::table('laporan_barangs');

        if ($request->filter_date) {

            if ($request->date && $request->filter_date == 1) {
                $query->whereDay('created_at', $request->date);
            }
            elseif ($request->filter_date == 2) {
                $query->whereBetween('created_at', [Carbon::now()->startOfWeek(), Carbon::now()->endOfWeek()]);
            }
            elseif ($request->month && $request->filter_date == 3) {
                $query->whereMonth('created_at', $request->month);
            }
            elseif ($request->year && $request->filter_date == 4) {
                $query->whereMonth('created_at', $request->year);
            }
        }

        $barang = $query->get();
        return response()->json($barang,200);
    }

    public function getLaporanBarangById($id)
    {

        $checking_role = $this->guard()->user();

        if ($checking_role->role == 0) {
            return response()->json([
                'message'=>'Unauthorized'
            ],401);
        }

        $barang =  DB::table('laporan_barangs')->where('id_barang', $id)->get();
        if ($barang->isEmpty()) {
            return response()->json([
                'message'=>'Barang Tidak ditemukan'
            ],404);
        }
        return response()->json($barang,200);
    }
    
    public function addBarang(Request $request)
    {
        $checking_role = $this->guard()->user();

        if ($checking_role->role == 1) {
            return response()->json([
                'message'=>'Unauthorized'
            ],401);
        }

        $barang =  Barang::create($request->all());
        DB::table('laporan_barangs')->insert([
            'id_barang' => $barang->id,
            'status_barang' => 0,
            'nilai' => $request->stok,
            'stok' => $request->stok,
            'created_at' => date('Y-m-d H:i:s')
        ]);
        return response()->json($barang,201);
    }
    
    public function updateStock(Request $request,$id)
    {

        $checking_role = $this->guard()->user();

        if ($checking_role->role == 1) {
            return response()->json([
                'message'=>'Unauthorized'
            ],401);
        }

            $update_stok = $request->stok;
            $barang =  DB::table('barangs')->where('id_barang', $id)->get();
            if ($barang->isEmpty()) {
                return response()->json([
                    'message'=>'Barang Tidak ditemukan'
                ],404);
            }
            $stok_lama = $barang[0]->stok;
            $stock_baru = $stok_lama + $update_stok;
            if ($stock_baru < 0) {
                return response()->json([
                    'message'=>'Stok tidak boleh kurang dari 0'
                ],404);
            }
            $updated_stock = DB::table('barangs')->where('id_barang', $id)->update([
                'stok' => $stock_baru,
                'updated_at' => date('Y-m-d H:i:s')
            ]);
            
            if ($updated_stock) {
                $barang_updated =  DB::table('barangs')->where('id_barang', $id)->get();
                $id_barang_laporan =  $barang_updated[0]->id_barang;
                $stok_laporan =  $barang_updated[0]->stok;

                if ($update_stok > 0) {
                    DB::table('laporan_barangs')->insert([
                        'id_barang' => $id_barang_laporan,
                        'status_barang' => 0,
                        'nilai' => $update_stok,
                        'stok' => $stok_laporan,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                }else {
                    DB::table('laporan_barangs')->insert([
                        'id_barang' => $id_barang_laporan,
                        'status_barang' => 1,
                        'nilai' => $update_stok,
                        'stok' => $stok_laporan,
                        'created_at' => date('Y-m-d H:i:s')
                    ]);
                }
                return response()->json([
                    'message'=>'Stok Telah diperbaharui',
                    'detail'=> $barang_updated
                ],200);
            }
    }

    protected function guard()
    {
        return Auth::guard();

    }//end guard()
}


